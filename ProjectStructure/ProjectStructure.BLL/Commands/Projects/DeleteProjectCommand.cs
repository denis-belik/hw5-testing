﻿using ProjectStructure.BLL.Abstractions.Commands;

namespace ProjectStructure.BLL.Commands.Projects
{
	public class DeleteProjectCommand : DeleteCommand
	{
		public int ProjectId { get; set; }
	}
}
