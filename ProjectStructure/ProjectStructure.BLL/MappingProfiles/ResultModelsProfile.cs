﻿using AutoMapper;
using ProjectStructure.Common.DTO.ResultModels;
using ProjectStructure.DAL.Models.ResultModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BLL.MappingProfiles
{
	public class ResultModelsProfile : Profile
	{
		public ResultModelsProfile()
		{
			CreateMap<ProjectTasksTeamInfo, ProjectTasksTeamInfoDTO>();
			CreateMap<UserProjectTasksInfo, UserProjectTasksInfoDTO>();
		}
	}
}
