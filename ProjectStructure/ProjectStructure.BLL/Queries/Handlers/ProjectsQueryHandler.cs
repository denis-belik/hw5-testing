﻿using AutoMapper;
using ProjectStructure.BLL.Abstractions.Queries;
using ProjectStructure.BLL.Abstractions.Queries.Handlers;
using ProjectStructure.DAL.Context;
using System.Collections.Generic;
using System.Linq;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.DAL.Models;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.ResultModels;
using ProjectStructure.BLL.Queries.Projects;
using ProjectStructure.Common.DTO.Task;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Models.ResultModels;

namespace ProjectStructure.BLL.Queries.Handlers
{
	public class ProjectsQueryHandler : QueryHandler<ProjectDTO>
	{
		public ProjectsQueryHandler(CompanyDbContext context, IMapper mapper)
			: base(context, mapper)
		{ }

		public override ProjectDTO Handle(GetByIdQuery query)
		{
			var project = _context.Projects.FirstOrDefault(project => project.Id == query.EntityId);

			if (project == null)
			{
				throw new NotFoundException(nameof(Project), query.EntityId);
			}

			return _mapper.Map<ProjectDTO>(project);
		}

		public override IEnumerable<ProjectDTO> Handle(GetAllQuery _)
		{
			var projects = _context.Projects;
			return _mapper.Map<ICollection<ProjectDTO>>(projects);
		}

		// 7. Отримати таку структуру:
		// Проект
		// Найдовший таск проекту(за описом)
		// Найкоротший таск проекту(по імені)
		// Загальна кількість користувачів в команді проекту, де або опис проекту >20 символів, або кількість тасків <3
		public IEnumerable<ProjectTasksTeamInfoDTO> Handle(GetProjectTasksTeamInfosQuery query)
		{
			var projectTasksTeamInfo =
				_context.Projects
				.Include(project => project.Tasks)
				.Include(project => project.Team)
					.ThenInclude(team => team.Members)
				.ToList()
				.Select(project => new ProjectTasksTeamInfo
				{
					Project = project,

					LongestDescriptionTask = 
						project.Tasks?.FirstOrDefault(
										task => task.Description.Length ==
												project.Tasks.Max(task => task.Description.Length)),

					ShortestNameTask =
						project.Tasks?.FirstOrDefault(
										task => task.Name.Length ==
												project.Tasks.Min(task => task.Name.Length)),

					TeamMembersCount =
						project.Description.Length > query.MinDescriptionLength || project.Tasks?.Count() < query.MaxTaskCount ?
						project.Team.Members.Count() :
						0
				});

			return _mapper.Map<IEnumerable<ProjectTasksTeamInfoDTO>>(projectTasksTeamInfo);

		}
	}
}
