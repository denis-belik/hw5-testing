﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Abstractions.Queries;
using ProjectStructure.BLL.Abstractions.Queries.Handlers;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Queries.Tasks;
using ProjectStructure.Common.DTO.ResultModels;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Queries.Handlers
{
	public class TasksQueryHandler : QueryHandler<TaskDTO>
	{
		public TasksQueryHandler(CompanyDbContext context, IMapper mapper)
			: base(context, mapper)
		{ }

		public override TaskDTO Handle(GetByIdQuery query)
		{
			var task = _context.Tasks.FirstOrDefault(task => task.Id == query.EntityId);

			if (task == null)
			{
				throw new NotFoundException(nameof(Task), query.EntityId);
			}

			return _mapper.Map<TaskDTO>(task);
		}

		public override IEnumerable<TaskDTO> Handle(GetAllQuery query)
		{
			var tasks = _context.Tasks;
			return _mapper.Map<ICollection<TaskDTO>>(tasks);
		}

		// 2. Отримати список тасків, призначених для конкретного користувача (по id), де name таска <45 символів (колекція з тасків).
		public IEnumerable<TaskDTO> Handle(GetTasksByNameLengthQuery query)
		{
			if (!_context.Users.Any(user => user.Id == query.PerformerId))
			{
				throw new NotFoundException(nameof(User), query.PerformerId);
			}

			var tasks = _context.Tasks
				.Include(task => task.Performer)
				.Where(task => task.PerformerId == query.PerformerId && task.Name.Length < query.MaxTaskNameLegth)
				.ToList();

			return _mapper.Map<ICollection<TaskDTO>>(tasks);
		}

		// 3. Отримати список (id, name) з колекції тасків, які виконані (finished) в поточному (2020) році для конкретного користувача (по id).
		public IEnumerable<TaskIdentitiesDTO> Handle(GetFinishedTaskIdentitiesQuery query)
		{
			if (!_context.Users.Any(user => user.Id == query.PerformerId))
			{
				throw new NotFoundException(nameof(User), query.PerformerId);
			}

			return _context.Tasks
				.Where(task =>
					task.PerformerId == query.PerformerId &&
					task.State == TaskState.Finished &&
					task.FinishedAt.Year == query.YearFinished)
				.Select(task => new TaskIdentitiesDTO { Id = task.Id, Name = task.Name });
		}

		//  Вибірка всіх невиконаних завдань для користувача (з усіх проектів)
		public IEnumerable<TaskDTO> Handle(GetNotFinishedUserTasksQuery query)
		{
			if(!_context.Users.Any(user => user.Id == query.PerformerId))
			{
				throw new NotFoundException(nameof(User), query.PerformerId);
			}

			var tasks = _context.Tasks
				.Where(task => task.PerformerId == query.PerformerId)
				.Where(task => task.State != TaskState.Finished);

			return _mapper.Map<ICollection<TaskDTO>>(tasks);
		}
	}
}
