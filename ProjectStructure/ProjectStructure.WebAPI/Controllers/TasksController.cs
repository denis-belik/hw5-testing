﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectStructure.BLL.Abstractions.Queries;
using ProjectStructure.BLL.Commands.Handlers;
using ProjectStructure.BLL.Commands.Tasks;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Queries.Handlers;
using ProjectStructure.BLL.Queries.Tasks;
using ProjectStructure.Common.DTO.Task;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class TasksController : ControllerBase
	{
		TasksQueryHandler _queryHandler;
		TasksCommandHandler _commandHandler;

		public TasksController(TasksQueryHandler queryHandler, TasksCommandHandler commandHandler)
		{
			_queryHandler = queryHandler;
			_commandHandler = commandHandler;
		}

		[HttpGet]
		public IActionResult Get()
		{
			try
			{
				var tasksDto = _queryHandler.Handle(new GetAllQuery());
				return Ok(tasksDto);
			}
			catch(Exception ex)
			{
				return StatusCode(500, ex.Message);
			}		
		}

		[HttpGet("{id}")]
		public IActionResult Get(int id)
		{
			try
			{
				var taskDto = _queryHandler.Handle(new GetByIdQuery(id));
				return Ok(taskDto);
			}
			catch(NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}		
		}

		[HttpGet("ByNameLength")]
		public IActionResult GetTasksByNameLength(int userId, int maxTaskNameLength = 45)
		{
			try
			{
				var tasks = _queryHandler.Handle(new GetTasksByNameLengthQuery
				{
					PerformerId = userId,
					MaxTaskNameLegth = maxTaskNameLength
				});
				return Ok(tasks);
			}
			catch (NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}		
		}

		[HttpGet("FinishedTaskIdentities")]
		public IActionResult GetFinishedTaskIdentities(int userId, int yearFinished = 2020)
		{
			try
			{
				var taskIdentities = _queryHandler.Handle(new GetFinishedTaskIdentitiesQuery
				{
					PerformerId = userId,
					YearFinished = yearFinished
				});
				return Ok(taskIdentities);
			}
			catch (NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}		
		}

		[HttpGet("NotFinished")]
		public IActionResult GetNotFinishedUserTasks(int performerId)
		{
			try
			{
				var tasks = _queryHandler.Handle(new GetNotFinishedUserTasksQuery { PerformerId = performerId });
				return Ok(tasks);
			}
			catch (NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}		
		}

		[HttpPost]
		public async Task<IActionResult> Post([FromBody] TaskCreateDTO taskDto)
		{
			try
			{
				int createdId = await _commandHandler.Handle(new AddTaskCommand { TaskCreateDto = taskDto });
				return Created($"api/tasks/{createdId}", taskDto);
			}
			catch (NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (ArgumentException ex)
			{
				return BadRequest(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}	
		}

		[HttpPut]
		public async Task<IActionResult> Put([FromBody] TaskUpdateDTO taskUpdateDto)
		{
			try
			{
				await _commandHandler.Handle(new UpdateTaskCommand { TaskUpdateDto = taskUpdateDto });
				return NoContent();
			}
			catch (NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (ArgumentException ex)
			{
				return BadRequest(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}
			
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(int id)
		{
			try
			{
				await _commandHandler.Handle(new DeleteTaskCommand { TaskId = id });
				return NoContent();
			}
			catch (NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}		
		}
	}
}
