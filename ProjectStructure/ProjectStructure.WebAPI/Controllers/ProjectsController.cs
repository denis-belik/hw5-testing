﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Abstractions.Queries;
using ProjectStructure.BLL.Commands.Handlers;
using ProjectStructure.BLL.Commands.Projects;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Queries.Handlers;
using ProjectStructure.BLL.Queries.Projects;
using ProjectStructure.Common.DTO.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ProjectsController : ControllerBase
	{
		ProjectsQueryHandler _queryHandler;
		ProjectsCommandHandler _commandHandler;

		public ProjectsController(ProjectsQueryHandler queryHandler, ProjectsCommandHandler commandHandler)
		{
			_queryHandler = queryHandler;
			_commandHandler = commandHandler;
		}

		[HttpGet]
		public IActionResult Get()
		{
			try
			{
				var projectsDto = _queryHandler.Handle(new GetAllQuery());
				return Ok(projectsDto);
			}
			catch(Exception ex)
			{
				return StatusCode(500, ex.Message);
			}		
		}

		[HttpGet("TasksTeamInfos")]
		public IActionResult Get(int minDescriptionLength = 20, int maxTasksCount = 3)
		{
			try
			{
				var projectsDto = _queryHandler.Handle(new GetProjectTasksTeamInfosQuery
				{
					MinDescriptionLength = minDescriptionLength,
					MaxTaskCount = maxTasksCount
				});
				return Ok(projectsDto);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}		
		}

		[HttpGet("{id}")]
		public IActionResult Get(int id)
		{
			try
			{
				var projectDto = _queryHandler.Handle(new GetByIdQuery(id));
				return Ok(projectDto);
			}
			catch (NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}	
		}

		[HttpPost]
		public async Task<IActionResult> Post([FromBody] ProjectCreateDTO projectDto)
		{
			try
			{
				int createdId = await _commandHandler.Handle(new AddProjectCommand { ProjectCreateDto = projectDto });
				return Created($"api/projects/{createdId}", projectDto);
			}
			catch (NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch(ArgumentException ex)
			{
				return BadRequest(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}		
		}

		[HttpPut]
		public async Task<IActionResult> Put([FromBody] ProjectUpdateDTO projectUpdateDto)
		{
			try
			{
				await _commandHandler.Handle(new UpdateProjectCommand { ProjectUpdateDto = projectUpdateDto });
				return NoContent();
			}
			catch (NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (ArgumentException ex)
			{
				return BadRequest(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}		
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(int id)
		{
			try
			{
				await _commandHandler.Handle(new DeleteProjectCommand { ProjectId = id });
				return NoContent();
			}
			catch (NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}		
		}
	}
}
