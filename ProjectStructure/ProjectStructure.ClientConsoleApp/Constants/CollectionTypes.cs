﻿

namespace ProjectStructure.ClientConsoleApp.Constants
{
	public static class CollectionTypes
	{
		public const string User = "user";
		public const string Project = "project";
		public const string Task = "task";
		public const string Team = "team";
	}
}
