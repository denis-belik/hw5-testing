﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ProjectStructure.DAL.Json
{
	public static class Deserializator
	{
		public static List<T> DeserializeCollection<T>(string path, string fileName)
		{
			List<T> collection = null;

			using (StreamReader reader = new StreamReader($@"{path}\{fileName}.json"))
			{
				string json = reader.ReadToEnd();
				collection = JsonConvert.DeserializeObject<List<T>>(json);
			}

			return collection;
		}
	}
}
