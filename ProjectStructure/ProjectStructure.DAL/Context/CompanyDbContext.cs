﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Json;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ProjectStructure.DAL.Context
{
	public class CompanyDbContext : DbContext
	{
		public CompanyDbContext(DbContextOptions<CompanyDbContext> options)
			: base(options)
		{

		}

		public DbSet<User> Users { get; set; }
		public DbSet<Project> Projects { get; set; }
		public DbSet<Task> Tasks { get; set; }
		public DbSet<Team> Teams { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Team>()
				.Property(team => team.Name)
				.HasColumnName("TeamName");

            DataSeeder.Seed(modelBuilder);
		}
	}
}
