﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.DAL.Models.ResultModels
{
	public class ProjectTasksTeamInfo
	{
		public Project Project { get; set; }
		public Task LongestDescriptionTask { get; set; }
		public Task ShortestNameTask { get; set; }
		public int TeamMembersCount { get; set; }
	}
}
