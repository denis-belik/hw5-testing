﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.DAL.Models
{
	public class Task
	{
		public int Id { get; set; }

		[Required, StringLength(128)]
		public string Name { get; set; }

		[Required, StringLength(1000)]
		public string Description { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime FinishedAt { get; set; }

		[Range(0, 3)]
		public TaskState State { get; set; }
		public int ProjectId { get; set; }
		public int PerformerId { get; set; }

		public User Performer { get; set; }
		public Project Project { get; set; }

		public Task() { }
		public Task(Task task)
		{
			Id = task.Id;
			Name = task.Name;
			Description = task.Description;
			CreatedAt = task.CreatedAt;
			FinishedAt = task.FinishedAt;
			State = task.State;
			ProjectId = task.ProjectId;
			PerformerId = task.PerformerId;

			Project = task.Project;
			Performer = task.Performer;
		}

		public Task(Task task, Project project) : this(task)
		{
			Project = project;
		}

		public Task(Task task, User performer) : this(task)
		{
			Performer = performer;
		}	
	}
}
