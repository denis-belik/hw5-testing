﻿
namespace ProjectStructure.DAL.Models
{
	public class TaskStateModel
	{
		public int Id { get; set; }
		public string Value { get; set; }
	}
}
