﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.DAL.Models
{
	public class Team
	{
		public int Id { get; set; }

		[Required, StringLength(32)]
		public string Name { get; set; }

		public DateTime CreatedAt { get; set; }

		public IEnumerable<User> Members { get; set; }

		public Team() { }
		public Team(Team team, IEnumerable<User> members)
		{
			Id = team.Id;
			Name = team.Name;
			CreatedAt = team.CreatedAt;

			Members = members;
		}
	}
}
