﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using Microsoft.Extensions.DependencyInjection;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.WebAPI.IntegrationTests
{
	public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                // Remove the app's ApplicationDbContext registration.
                var descriptor = services.FirstOrDefault(
                    d => d.ServiceType ==
                        typeof(DbContextOptions<CompanyDbContext>));

                if (descriptor != null)
                {
                    services.Remove(descriptor);
                }

                // Add ApplicationDbContext using an in-memory database for testing.
                services.AddDbContext<CompanyDbContext>(options =>
                {
                    options.UseInMemoryDatabase("TestCompanyDatabase3");
                });

                // Build the service provider.
                var sp = services.BuildServiceProvider();

                // Create a scope to obtain a reference to the database
                // context (ApplicationDbContext).
                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var context = scopedServices.GetRequiredService<CompanyDbContext>();

                    // Ensure the database is created.
                    context.Database.EnsureCreated();

                    var projects = context.Projects.ToList();
                    context.Projects.RemoveRange(projects);

                    var users = context.Users.ToList();
                    context.Users.RemoveRange(users);

                    var tasks = context.Tasks.ToList();
                    context.Tasks.RemoveRange(tasks);

                    var teams = context.Teams.ToList();
                    context.Teams.RemoveRange(teams);

                    context.SaveChanges();

                    // Seed the database with test data.
                    SeedData(context);                 
                }
            });
        }

        private void SeedData(CompanyDbContext context)
        {
            var userToDelete = new User
            {
                Id = 1,
                FirstName = "Maynard",
                LastName = "Keenan",
                Email = "mkeenan@gmail.com",
                City = "Los Angeles",
                Birthday = new DateTime(1964, 5, 17),
                RegisteredAt = new DateTime(2020, 1, 1),
                TeamId = 1
            };

            var userAuthorForCreatedProject = new User
            {
                Id = 11
            };

            var projectToCascadeDelete = new Project
            {
                Id = 11
            };

            var teamForCreatedProject = new Team
            {
                Id = 11
            };

            var taskToDelete = new Task
            {
                Id = 2,
                Name = "Task 2",
                Description = "Task 2 description",
                CreatedAt = new DateTime(2020, 1, 1),
                FinishedAt = new DateTime(2020, 7, 19),
                ProjectId = 1,
                PerformerId = 1
            };
            context.AddRange(userToDelete, userAuthorForCreatedProject, projectToCascadeDelete, teamForCreatedProject, taskToDelete);
            context.SaveChanges();
        }
    }
}
