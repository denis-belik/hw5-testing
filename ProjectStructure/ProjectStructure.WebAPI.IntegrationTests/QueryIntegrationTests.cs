using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProjectStructure.Common.DTO.ResultModels;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ProjectStructure.WebAPI.IntegrationTests
{
	public class CommandIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
	{
		private readonly HttpClient _client;
		private readonly CustomWebApplicationFactory<Startup> _factory;

		public CommandIntegrationTests(CustomWebApplicationFactory<Startup> factory)
		{
			_factory = factory;
			_client = factory.CreateClient(new WebApplicationFactoryClientOptions
			{
				AllowAutoRedirect = false
			});
		}

		#region PROJECT CREATION
		[Theory]
		[InlineData("{\"name\": \"Project 1 integration\", \"description\": \"Project 1 description\", \"deadline\": \"2020-11-07T14:49:38.5028811+02:00\", \"authorId\": 11, \"teamId\": 11}")]
		public async System.Threading.Tasks.Task PostNewProject_WhenProjectCreateDTOJsonIsCorrect_ThenCodeStatus201(string jsonInString)
		{
			var response = await _client.PostAsync("api/projects", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

			Assert.Equal(HttpStatusCode.Created, response.StatusCode);

			using (var scope = _factory.Services.CreateScope())
			{
				var context = scope.ServiceProvider.GetRequiredService<CompanyDbContext>();
				Assert.Contains(context.Projects, project => project.Name == "Project 1 integration");
			}
		}

		[Theory]
		[InlineData("{\"name\": \"Project 2 integration\", \"description\": \"Project 2 description\", \"deadline\": \"2020-11-07T14:49:38.5028811+02:00\", \"authorId\": 291827381, \"teamId\": 11}")]
		public async System.Threading.Tasks.Task PostNewProject_WhenAuthorNotFound_ThenCodeStatus404(string jsonInString)
		{
			var response = await _client.PostAsync("api/projects", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

			Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

			using (var scope = _factory.Services.CreateScope())
			{
				var context = scope.ServiceProvider.GetRequiredService<CompanyDbContext>();
				Assert.DoesNotContain(context.Projects, project => project.Name == "Project 2 integration");
			}
		}
		#endregion

		#region USER DELETION
		[Fact]
		public async System.Threading.Tasks.Task DeleteUser_WhenUserExists_ThenCodeStatus204()
		{
			using (var scope = _factory.Services.CreateScope())
			{
				var context = scope.ServiceProvider.GetRequiredService<CompanyDbContext>();
				int userId = 1;
				var response = await _client.DeleteAsync($"api/users/{userId}");

				Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);

				
				Assert.DoesNotContain(context.Users, user => user.Id == 1);
				Assert.Contains(context.Projects, project => project.Id == 11); // cascade delete does not work
			}
		}

		[Fact]
		public async System.Threading.Tasks.Task DeleteUser_WhenUserDoesNotExist_ThenCodeStatus404()
		{
			int userId = int.MaxValue;
			var response = await _client.DeleteAsync($"api/users/{userId}");

			Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
		}
		#endregion

		#region TEAM CREATION
		[Theory]
		[InlineData("{\"name\": \"Team 1 integration\", \"createdAt\": \"1920-07-01T06:02:23.1538591+03:00\"}")]
		public async System.Threading.Tasks.Task PostNewTeam_WhenTEamCreateDTOJsonIsCorrect_ThenCodeStatus201(string jsonInString)
		{
			var response = await _client.PostAsync("api/teams", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

			Assert.Equal(HttpStatusCode.Created, response.StatusCode);

			using (var scope = _factory.Services.CreateScope())
			{
				var context = scope.ServiceProvider.GetRequiredService<CompanyDbContext>();
				Assert.Contains(context.Teams, team => team.Name == "Team 1 integration");
			}
		}

		[Theory]
		[InlineData("{\"name\": \"\", \"createdAt\": \"1920-07-01T06:02:23.1538591+03:00\"}")]
		public async System.Threading.Tasks.Task PostNewTeam_WhenTeamCreateDTOJsonIsIncorrect_ThenCodeStatus400(string jsonInString)
		{
			var response = await _client.PostAsync("api/teams", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

			Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
		}
		#endregion

		#region TASK 3
		private void SeedDataForTask3(CompanyDbContext context)
		{
			var user = new User
			{
				Id = 31
			};

			// matching tasks
			var task1 = new DAL.Models.Task
			{
				Id = 31,
				Name = $"Task of user {user.Id}",
				State = TaskState.Finished,
				FinishedAt = new DateTime(2020, 3, 1),
				PerformerId = user.Id
			};
			var task2 = new DAL.Models.Task
			{
				Id = 32,
				Name = $"Task of user {user.Id}",
				State = TaskState.Finished,
				FinishedAt = new DateTime(2020, 3, 1),
				PerformerId = user.Id
			};
			// not matching tasks
			var task3 = new DAL.Models.Task
			{
				Id = 33,
				Name = "Task 33",
				State = TaskState.Finished,
				FinishedAt = new DateTime(2021, 3, 1),
				PerformerId = 31
			};
			var task4 = new DAL.Models.Task
			{
				Id = 34,
				Name = "Task 34",
				State = TaskState.Canceled,
				FinishedAt = new DateTime(2020, 3, 1),
				PerformerId = 31
			};
			var task5 = new DAL.Models.Task
			{
				Id = 35,
				Name = "Task 35",
				State = TaskState.Canceled,
				FinishedAt = new DateTime(2021, 3, 1),
				PerformerId = 31
			};
			var task6 = new DAL.Models.Task
			{
				Id = 36,
				Name = "Task 36",
				State = TaskState.Finished,
				FinishedAt = new DateTime(2020, 3, 1),
				PerformerId = -1
			};

			context.AddRange(user, task1, task2, task3, task4, task5, task6);
			context.SaveChanges();
		}

		[Fact]
		public async System.Threading.Tasks.Task GetFinishedTaskIdentities_WhenUserExistsAndOnly2TasksMatchTheRequirements_ThenCollectionOfTwoTaskIdentities()
		{
			using (var scope = _factory.Services.CreateScope())
			{
				var context = scope.ServiceProvider.GetRequiredService<CompanyDbContext>();
				SeedDataForTask3(context);

				int userId = 31;
				var response = await _client.GetAsync($"api/tasks/FinishedTaskIdentities?userId={userId}");

				Assert.Equal(HttpStatusCode.OK, response.StatusCode);

				var jsonInString = await response.Content.ReadAsStringAsync();
				var taskIdentities = JsonConvert.DeserializeObject<List<TaskIdentitiesDTO>>(jsonInString);

				Assert.Collection(taskIdentities,
					task =>
					{
						Assert.InRange<int>(task.Id, 31, 32);
					},
					task =>
					{
						Assert.InRange<int>(task.Id, 31, 32);
					}
				);

				context.Database.EnsureDeleted();
			}
		}

		[Fact]
		public async System.Threading.Tasks.Task GetFinishedTaskIdentities_WhenUserNotFound_ThenThrowNotFoundException()
		{
			using (var scope = _factory.Services.CreateScope())
			{
				var context = scope.ServiceProvider.GetRequiredService<CompanyDbContext>();
				SeedDataForTask3(context);

				int userId = int.MaxValue;
				var response = await _client.GetAsync($"api/tasks/FinishedTaskIdentities?userId={userId}");

				Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

				context.Database.EnsureDeleted();
			}
		}
		#endregion

		#region TASK DELETION
		[Fact]
		public async System.Threading.Tasks.Task DeleteTask_WhenTaskExists_ThenCodeStatus204()
		{
			using (var scope = _factory.Services.CreateScope())
			{
				var context = scope.ServiceProvider.GetRequiredService<CompanyDbContext>();
				var taskToDelete = new DAL.Models.Task
				{
					Id = 2,
					Name = "Task 2",
					Description = "Task 2 description",
					CreatedAt = new DateTime(2020, 1, 1),
					FinishedAt = new DateTime(2020, 7, 19),
					ProjectId = 1,
					PerformerId = 1
				};
				context.Add(taskToDelete);
				context.SaveChanges();

				int taskId = 2;
				var response = await _client.DeleteAsync($"api/tasks/{taskId}");

				Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);

				Assert.DoesNotContain(context.Tasks, task => task.Id == 2);
			}
		}

		[Fact]
		public async System.Threading.Tasks.Task DeleteTask_WhenTaskDoesNotExist_ThenCodeStatus404()
		{
			int taskId = int.MaxValue;
			var response = await _client.DeleteAsync($"api/task/{taskId}");

			Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
		}
		#endregion

		#region USER'S NOT FINISHED TASKS
		private void SeedDataForNotFinishedUserTasks(CompanyDbContext context)
		{
			var user = new User
			{
				Id = 81
			};

			var notFinishedTask1 = new DAL.Models.Task
			{
				Id = 81,
				State = TaskState.Created,
				PerformerId = 81
			};

			var notFinishedTask2 = new DAL.Models.Task
			{
				Id = 82,
				State = TaskState.Started,
				PerformerId = 81
			};

			var notFinishedTask3 = new DAL.Models.Task
			{
				Id = 83,
				State = TaskState.Started,
				PerformerId = -1
			};

			var finishedTask = new DAL.Models.Task
			{
				Id = 84,
				State = TaskState.Finished,
				PerformerId = 81
			};

			context.AddRange(user, notFinishedTask1, notFinishedTask2, notFinishedTask3, finishedTask);
			context.SaveChanges();
		}

		[Fact]
		public async System.Threading.Tasks.Task GetNotFinishedUserTasks_WhenUserHasThreeTasksAndTwoOfThemNotFinished_ThenStatusCode200AndCollectionOfTwoTasks()
		{
			using (var scope = _factory.Services.CreateScope())
			{
				var context = scope.ServiceProvider.GetRequiredService<CompanyDbContext>();
				SeedDataForNotFinishedUserTasks(context);

				int userId = 81;
				var response = await _client.GetAsync($"api/tasks/NotFinished?performerId={userId}");

				Assert.Equal(HttpStatusCode.OK, response.StatusCode);

				string jsonInString = await response.Content.ReadAsStringAsync();

				var notFinishedTasks = JsonConvert.DeserializeObject<List<TaskDTO>>(jsonInString);

				Assert.Collection(notFinishedTasks,
					task =>
					{
						Assert.InRange(task.Id, 81, 82);
						Assert.Equal(userId, task.PerformerId);
						Assert.NotEqual(TaskState.Finished, task.State);
					},
					task =>
					{
						Assert.InRange(task.Id, 81, 82);
						Assert.Equal(userId, task.PerformerId);
						Assert.NotEqual(TaskState.Finished, task.State);
					}
				);
			}
		}

		[Fact]
		public async System.Threading.Tasks.Task GetNotFinishedUserTasks_WhenUserDoesNotExist_ThenStatusCode404()
		{
			int userId = int.MaxValue;
			var response = await _client.GetAsync($"api/tasks/NotFinished?performerId={userId}");

			Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
		}
		#endregion
	}
}
