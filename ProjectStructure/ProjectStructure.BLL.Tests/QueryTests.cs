using AutoMapper;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Abstractions.Queries;
using ProjectStructure.BLL.Abstractions.Queries.Handlers;
using ProjectStructure.BLL.Commands.Handlers;
using ProjectStructure.BLL.Commands.Users;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.MappingProfiles;
using ProjectStructure.BLL.Queries.Handlers;
using ProjectStructure.BLL.Queries.Projects;
using ProjectStructure.BLL.Queries.Tasks;
using ProjectStructure.BLL.Queries.Teams;
using ProjectStructure.BLL.Queries.Users;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace ProjectStructure.BLL.Tests
{
	public class QueryTests
	{
		protected DbContextOptions<CompanyDbContext> ContextOptions { get; }
		private IMapper mapper;

		public QueryTests()
		{
			ContextOptions = new DbContextOptionsBuilder<CompanyDbContext>()
					.UseInMemoryDatabase("TestCompanyDatabase")
					.Options;

			mapper = new Mapper(new MapperConfiguration(
				cfg =>
				{
					cfg.AddProfile<UserProfile>();
					cfg.AddProfile<ProjectProfile>();
					cfg.AddProfile<TaskProfile>();
					cfg.AddProfile<TeamProfile>();
					cfg.AddProfile<ResultModelsProfile>();
				}
			));
		}

		private void SeedDataForTask1(FakeCompanyDbContext context)
		{
			var user = new User
			{
				Id = 11
			};
			var userWithoutProjects = new User
			{
				Id = 13
			};

			var project1 = new Project
			{
				Id = 11,
				AuthorId = 11
			};
			var project2 = new Project
			{
				Id = 12,
				AuthorId = 11,
			};
			var project3 = new Project
			{
				Id = 13,
				AuthorId = -1,
			};

			var task1 = new Task
			{
				Id = 11,
				ProjectId = 11,
			};
			var task2 = new Task
			{
				Id = 12,
				ProjectId = 11,
			};
			var task3 = new Task
			{
				Id = 13,
				ProjectId = 11
			};
			var task4 = new Task
			{
				Id = 14,
				ProjectId = 11
			};
			var task5 = new Task
			{
				Id = 15,
				ProjectId = -1
			};

			context.AddRange(user, userWithoutProjects, project1, project2, project3, task1, task2, task3, task4, task5);
			context.SaveChanges();
		}
		private void SeedDataForTask2(FakeCompanyDbContext context)
		{
			// ids in range [21-29]
			var user1 = new User
			{
				Id = 21,
			};

			var task1 = new Task
			{
				Id = 21,
				Name = "Task 1",
				PerformerId = 21
			};
			var task2 = new Task
			{
				Id = 22,
				Name = "Task 2",
				PerformerId = 21
			};
			var task3 = new Task
			{
				Id = 23,
				Name = "Task 3 1234567890 1234567890 1234567890 1234567890 1234567890",
				PerformerId = 21
			};

			context.AddRange(user1, task1, task2, task3);
			context.SaveChanges();
		}
		private void SeedDataForTask3(FakeCompanyDbContext context)
		{
			var user = new User
			{
				Id = 31
			};

			// matching tasks
			var task1 = new Task
			{
				Id = 31,
				Name = $"Task of user {user.Id}",
				State = TaskState.Finished,
				FinishedAt = new DateTime(2020, 3, 1),
				PerformerId = user.Id
			};
			var task2 = new Task
			{
				Id = 32,
				Name = $"Task of user {user.Id}",
				State = TaskState.Finished,
				FinishedAt = new DateTime(2020, 3, 1),
				PerformerId = user.Id
			};
			// not matching tasks
			var task3 = new Task
			{
				Id = 33,
				Name = "Task 33",
				State = TaskState.Finished,
				FinishedAt = new DateTime(2021, 3, 1),
				PerformerId = 31
			};
			var task4 = new Task
			{
				Id = 34,
				Name = "Task 34",
				State = TaskState.Canceled,
				FinishedAt = new DateTime(2020, 3, 1),
				PerformerId = 31
			};
			var task5 = new Task
			{
				Id = 35,
				Name = "Task 35",
				State = TaskState.Canceled,
				FinishedAt = new DateTime(2021, 3, 1),
				PerformerId = 31
			};
			var task6 = new Task
			{
				Id = 36,
				Name = "Task 36",
				State = TaskState.Finished,
				FinishedAt = new DateTime(2020, 3, 1),
				PerformerId = -1
			};

			context.AddRange(user, task1, task2, task3, task4, task5, task6);
			context.SaveChanges();
		}
		private void SeedDataForTask4(FakeCompanyDbContext context)
		{
			// users older than 10
			// users from team 41
			var user1 = new User
			{
				Id = 41,
				Birthday = new DateTime(2009, 1, 1),
				RegisteredAt = new DateTime(2020, 1, 1),
				TeamId = 41
			};
			var user2 = new User
			{
				Id = 42,
				Birthday = new DateTime(2009, 2, 1),
				RegisteredAt = new DateTime(2020, 2, 1),
				TeamId = 41
			};
			var user8 = new User
			{
				Id = 48,
				Birthday = new DateTime(2009, 8, 1),
				RegisteredAt = new DateTime(2020, 1, 8),
				TeamId = 41
			};
			// users from team 42
			var user4 = new User
			{
				Id = 44,
				Birthday = new DateTime(2009, 4, 1),
				RegisteredAt = new DateTime(2020, 4, 1),
				TeamId = 42
			};
			var user5 = new User
			{
				Id = 45,
				Birthday = new DateTime(2009, 5, 1),
				RegisteredAt = new DateTime(2020, 5, 1),
				TeamId = 42
			};

			// users yonger than 10
			var user3 = new User
			{
				Id = 43,
				Birthday = new DateTime(2011, 3, 1),
				RegisteredAt = new DateTime(2020, 3, 1),
				TeamId = 41
			};
			var user6 = new User
			{
				Id = 46,
				Birthday = new DateTime(2011, 6, 1),
				RegisteredAt = new DateTime(2020, 6, 1),
				TeamId = 42
			};

			// user from team that doesn't exist
			var user7 = new User
			{
				Id = 47,
				Birthday = new DateTime(2009, 7, 1),
				RegisteredAt = new DateTime(2020, 7, 1),
				TeamId = -1
			};

			var team1 = new Team
			{
				Id = 41,
				Name = "Team 41"
			};
			var team2 = new Team
			{
				Id = 42,
				Name = "Team 42"
			};

			// team without members
			var team3 = new Team
			{
				Id = 43,
				Name = "Team 43"
			};

			context.AddRange(user1, user2, user3, user4, user5, user6, user7, user8, team1, team2, team3);
			context.SaveChanges();
		}
		private void SeedDataForTask5(FakeCompanyDbContext context)
		{
			var user1 = new User
			{
				Id = 51,
				FirstName = "Aaa"
			};
			var user2 = new User
			{
				Id = 52,
				FirstName = "Baa"
			};
			var user3 = new User
			{
				Id = 53,
				FirstName = "Aba"
			};

			var task1 = new Task
			{
				Id = 51,
				Name = "123",
				PerformerId = 51
			};
			var task2 = new Task
			{
				Id = 52,
				Name = "1234",
				PerformerId = 51
			};
			var task3 = new Task
			{
				Id = 53,
				Name = "12345",
				PerformerId = 51
			};

			var task4 = new Task
			{
				Id = 54,
				Name = "Task 4",
				PerformerId = 52
			};

			context.AddRange(user1, user2, user3, task1, task2, task3, task4);
			context.SaveChanges();
		}
		private void SeedDataForTask6(FakeCompanyDbContext context)
		{
			var user = new User
			{
				Id = 61
			};

			var project1 = new Project
			{
				Id = 61,
				AuthorId = 61,
				CreatedAt = new DateTime(2020, 1, 1)
			};
			// latest project
			var project2 = new Project
			{
				Id = 62,
				AuthorId = 61,
				CreatedAt = new DateTime(2020, 3, 3)
			};
			var project3 = new Project
			{
				Id = 63,
				AuthorId = 61,
				CreatedAt = new DateTime(2020, 1, 1)
			};
			// not user's project
			var project4 = new Project
			{
				Id = 64,
				AuthorId = -1,
				CreatedAt = new DateTime(2020, 4, 4)
			};

			// 2019-2021, created
			var task1 = new Task
			{
				Id = 61,
				PerformerId = 61,
				ProjectId = 62,
				State = TaskState.Created,
				CreatedAt = new DateTime(2019, 1, 1),
				FinishedAt = new DateTime(2021, 1, 1)
			};
			// 2018-2021, started
			var task2 = new Task
			{
				Id = 62,
				PerformerId = 61,
				ProjectId = 62,
				State = TaskState.Started,
				CreatedAt = new DateTime(2018, 1, 1),
				FinishedAt = new DateTime(2021, 1, 1)
			};
			// 2017-2021, canceled
			var task3 = new Task
			{
				Id = 63,
				PerformerId = 61,
				ProjectId = 62,
				State = TaskState.Canceled,
				CreatedAt = new DateTime(2017, 1, 1),
				FinishedAt = new DateTime(2021, 1, 1)
			};
			// 2020-2021, finished
			var task4 = new Task
			{
				Id = 64,
				PerformerId = 61,
				ProjectId = 62,
				State = TaskState.Finished,
				CreatedAt = new DateTime(2020, 1, 1),
				FinishedAt = new DateTime(2021, 1, 1)
			};
			// not user's task
			var task5 = new Task
			{
				Id = 65,
				PerformerId = -1,
				ProjectId = -1,
				State = TaskState.Finished,
				CreatedAt = new DateTime(2000, 1, 1),
				FinishedAt = new DateTime(2021, 1, 1)
			};

			context.AddRange(user, project1, project2, project3, project4, task1, task2, task3, task4, task5);
			context.SaveChanges();
		}
		private void SeedDataForTask7(FakeCompanyDbContext context)
		{
			// task.ProjectId
			// project.TeamId
			// user.TeamId
			// Project
			// LongestDescriptionTask
			// ShortestNameTask
			// TeamMembersCount

			var project1 = new Project
			{
				Id = 71,
				TeamId = 71,
				Description = "1234567890 1234567890 1234567890"
			};
			var project2 = new Project
			{
				Id = 72,
				Description = "Short desc",
				TeamId = 72
			};
			// project without tasks
			var project3 = new Project
			{
				Id = 73,
				TeamId = 72,
				Description = "ne null"
			};

			//	project 71 tasks
			var task1 = new Task
			{
				Id = 71,
				Name = "Task 71 very long name",
				Description = "Task 71 description",
				ProjectId = 71
			};
			// longest description
			var task2 = new Task
			{
				Id = 72,
				Name = "Task 72",
				Description = "Task 72 description very long description",
				ProjectId = 71
			};
			// shortest name
			var task3 = new Task
			{
				Id = 73,
				Name = "Task 73",
				Description = "Task 71 description",
				ProjectId = 71
			};
			// project 72 tasks
			var task4 = new Task
			{
				Id = 74,
				ProjectId = 72,
				Name = "Task 74",
				Description = "Task 74 description"
			};

			var team1 = new Team
			{
				Id = 71
			};
			var team2 = new Team
			{
				Id = 72
			};

			// user from team 71
			var user1 = new User
			{
				Id = 71,
				TeamId = 71,
			};
			// user from team 72
			var user2 = new User
			{
				Id = 72,
				TeamId = 72,
			};

			context.AddRange(project1, project2, project3, task1, task2, task3, task4, team1, team2, user1, user2);
			context.SaveChanges();
		}
		private void SeedDataForNotFinishedUserTasks(FakeCompanyDbContext context)
		{
			var user = new User
			{
				Id = 81
			};

			var notFinishedTask1 = new Task
			{
				Id = 81,
				State = TaskState.Created,
				PerformerId = 81
			};

			var notFinishedTask2 = new Task
			{
				Id = 82,
				State = TaskState.Started,
				PerformerId = 81
			};
			// this task is not user's 81
			var notFinishedTask3 = new Task
			{
				Id = 83,
				State = TaskState.Started,
				PerformerId = -1
			};

			var finishedTask = new Task
			{
				Id = 84,
				State = TaskState.Finished,
				PerformerId = 81
			};

			context.AddRange(user, notFinishedTask1, notFinishedTask2, notFinishedTask3, finishedTask);
			context.SaveChanges();
		}


		#region TASK 1
		// 1. �������� ������� ����� � ������ ����������� �����������(�� id) (�������, �� key ���� ������, � value ������� �����).
		[Fact]
		public void GetProjectTasksCountByAuthorId_WhenAuthorExists_ThenDictionaryWithTwoElementsAndProjectIdsEqual1and2AndAuthorIdEquals1AndTasksCountEqual4and0()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();
				SeedDataForTask1(context);
				UsersQueryHandler usersQueryHandler = new UsersQueryHandler(context, mapper);

				int authorId = 11;

				var dictionary = usersQueryHandler.Handle(new GetProjectTasksCountQuery { ProjectAuthorId = authorId });

				Assert.Collection(dictionary,
					kvPair =>
					{
						Assert.Equal(authorId, kvPair.Key.AuthorId); // project's author id
						Assert.Equal(11, kvPair.Key.Id); // project id
						Assert.Equal(4, kvPair.Value); // tasks count in the projects
					},
					kvPair =>
					{
						Assert.Equal(authorId, kvPair.Key.AuthorId); // project's author id
						Assert.Equal(12, kvPair.Key.Id); // project id
						Assert.Equal(0, kvPair.Value); // tasks count in the projects
					}
				);
			}	
		}
		
		[Fact]
		public void GetProjectTasksCountByAuthorId_WhenAuthorNotFound_ThenThrowNotFoundException()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();
				SeedDataForTask1(context);
				UsersQueryHandler usersQueryHandler = new UsersQueryHandler(context, mapper);

				int authorId = int.MaxValue;

				Assert.Throws<NotFoundException>(
					() => usersQueryHandler.Handle(new GetProjectTasksCountQuery { ProjectAuthorId = authorId }));
			}
			
		}

		[Fact]
		public void GetProjectTasksCountByAuthorId_WhenAuthorHasNoProjects_ThenDictionaryIsEmpty()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();
				SeedDataForTask1(context);
				UsersQueryHandler usersQueryHandler = new UsersQueryHandler(context, mapper);

				int authorId = 13;

				var dictionary = usersQueryHandler.Handle(new GetProjectTasksCountQuery { ProjectAuthorId = authorId });

				Assert.Empty(dictionary);
			}	
		}
		#endregion

		#region TASK 2
		// 2. �������� ������ �����, ����������� ��� ����������� ����������� (�� id), �� name ����� <45 ������� (�������� � �����).
		[Fact]
		public void GetTasksByNameLength_WhenUserExistsAndHasThreeTasksButOneOfThemWithDescLengthMoreThan45_ThenCollectionOfTwoTasks()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();
				SeedDataForTask2(context);
				TasksQueryHandler tasksQueryHandler = new TasksQueryHandler(context, mapper);

				int performerId = 21;
				var tasks = tasksQueryHandler.Handle(new GetTasksByNameLengthQuery { PerformerId = performerId });
				Assert.Collection(tasks,
					task =>
					{
						Assert.Equal(performerId, task.PerformerId);
						Assert.Equal(21, task.Id);
						Assert.True(task.Name.Length < 45);
					},
					task =>
					{
						Assert.Equal(performerId, task.PerformerId);
						Assert.Equal(22, task.Id);
						Assert.True(task.Name.Length < 45);
					}
				);
			}
		}

		[Fact]
		public void GetTasksByNameLength_WhenUserNotFound_ThenThrowNotFoundException()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();
				SeedDataForTask2(context);
				TasksQueryHandler tasksQueryHandler = new TasksQueryHandler(context, mapper);

				int performerId = int.MaxValue;

				Assert.Throws<NotFoundException>(() => tasksQueryHandler.Handle(new GetTasksByNameLengthQuery { PerformerId = performerId }));
			}
			
		}

		#endregion

		#region TASK 3
		// 3. �������� ������ (id, name) � �������� �����, �� �������� (finished) � ��������� (2020) ���� ��� ����������� ����������� (�� id).
		[Fact]
		public void GetFinishedTaskIdentities_WhenUserExistsAndOnly2TasksMatchTheRequirements_ThenCollectionOfTwoTaskIdentities()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();
				SeedDataForTask3(context);
				TasksQueryHandler tasksQueryHandler = new TasksQueryHandler(context, mapper);

				int performerId = 31;
				var taskIdentities = tasksQueryHandler.Handle(new GetFinishedTaskIdentitiesQuery { PerformerId = performerId });

				Assert.Collection(taskIdentities,
					task =>
					{
						Assert.Equal($"Task of user {performerId}", task.Name);
						Assert.Equal(31, task.Id);
					},
					task =>
					{
						Assert.Equal($"Task of user {performerId}", task.Name);
						Assert.Equal(32, task.Id);

					}
				);
			}	
		}

		[Fact]
		public void GetFinishedTaskIdentities_WhenUserNotFound_ThenThrowNotFoundException()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();
				SeedDataForTask3(context);
				TasksQueryHandler tasksQueryHandler = new TasksQueryHandler(context, mapper);

				int performerId = int.MaxValue;

				Assert.Throws<NotFoundException>(() => tasksQueryHandler.Handle(new GetFinishedTaskIdentitiesQuery { PerformerId = performerId }));
			}
			
		}
		#endregion

		#region TASK 4
		// 4. �������� ������ (id, ��'� ������� � ������ ������������) � �������� ������, �������� ���� ������ 10 ����,
		// ������������ �� ����� ��������� ����������� �� ���������, � ����� ����������� �� ��������.
		[Fact]
		public void GetTeamMembersByAge_WhenTwoTeamsHaveThreeMembersEachAndTwoMembersFromEachTeamMatchRequirements_ThenCollectionOfTwoTeamsWithTwoMembersInEach()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();
				SeedDataForTask4(context);
				TeamsQueryHandler teamsQueryHandler = new TeamsQueryHandler(context, mapper);

				var teams = teamsQueryHandler.Handle(new GetTeamMembersByAgeQuery());

				Assert.Collection(teams,
					team =>
					{
						Assert.Collection(team.Members,
							user =>
							{
								Assert.Equal(team.Id, user.TeamId);
								Assert.True(DateTime.Now.Year - user.Birthday.Year > 10);
								Assert.Equal(45, user.Id);
							},
							user =>
							{
								Assert.Equal(team.Id, user.TeamId);
								Assert.True(DateTime.Now.Year - user.Birthday.Year > 10);
								Assert.Equal(44, user.Id);
							});
					},
					team =>
					{
						Assert.Collection(team.Members,
							user =>
							{
								Assert.Equal(team.Id, user.TeamId);
								Assert.True(DateTime.Now.Year - user.Birthday.Year > 10);
								Assert.Equal(42, user.Id);
							},
							user =>
							{
								Assert.Equal(team.Id, user.TeamId);
								Assert.True(DateTime.Now.Year - user.Birthday.Year > 10);
								Assert.Equal(48, user.Id);
							},
							user =>
							{
								Assert.Equal(team.Id, user.TeamId);
								Assert.True(DateTime.Now.Year - user.Birthday.Year > 10);
								Assert.Equal(41, user.Id);
							});
					});
			}
			
		}

		[Fact]
		public void GetTeamMembersByAge_WhenTwoTeamsHaveThreeMembersEachAndNoMemberMatchesRequirements_ThenEmptyColection()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();
				SeedDataForTask4(context);
				TeamsQueryHandler teamsQueryHandler = new TeamsQueryHandler(context, mapper);

				var teams = teamsQueryHandler.Handle(new GetTeamMembersByAgeQuery { MinAge = 1000 });
				Assert.Empty(teams);
			}
		}

		#endregion

		#region TASK 5
		// 5. �������� ������ ������������ �� �������� first_name(�� ���������) � ������������� tasks �� ������� name(�� ���������).
		[Fact]
		public void GetSortedUsersWithTasks_WhenThreeUsersWithTheirTasks_ThenOrderedCollectionCollectionOfThreeUsersAndTheirOrderedTasks()
		{
			using(var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();
				SeedDataForTask5(context);

				UsersQueryHandler usersQueryHandler = new UsersQueryHandler(context, mapper);
				var users = usersQueryHandler.Handle(new GetSortedUsersWithTasksQuery());

				Assert.Collection(users,
					user =>
					{
						Assert.Equal("Aaa", user.FirstName);
						Assert.Collection(user.Tasks,
							task => { Assert.Equal(53, task.Id); },
							task => { Assert.Equal(52, task.Id); },
							task => { Assert.Equal(51, task.Id); }
						);
					},
					user =>
					{
						Assert.Equal("Aba", user.FirstName);
						Assert.Empty(user.Tasks);
					},
					user =>
					{
						Assert.Equal("Baa", user.FirstName);
						Assert.Collection(user.Tasks,
							task => { Assert.Equal(54, task.Id); }
						);
					}
				);
			}
		}
		#endregion

		#region TASK 6
		// 6. �������� �������� ��������� (�������� Id ����������� � ���������):
		// User
		// �������� ������ ����������� (�� ����� ���������)
		// �������� ������� ����� �� �������� ��������
		// �������� ������� ������������ ��� ���������� ����� ��� �����������
		// ������������ ���� ����������� �� ����� (��������� ��������� - ��������� ���������)
		[Fact]
		public void GetUserProjectTasksInfo_WhenUserHasTwoProjectsAndFourTasksThreeOfThemNotFinished_ThenGetStructure()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();
				SeedDataForTask6(context);
				UsersQueryHandler usersQueryHandler = new UsersQueryHandler(context, mapper);

				int userId = 61;
				var structure = usersQueryHandler.Handle(new GetUserProjectTasksInfoQuery { UserId = userId });

				Assert.Equal(userId, structure.User.Id);

				Assert.Equal(62, structure.LatestProject.Id);
				Assert.Equal(userId, structure.LatestProject.AuthorId);

				Assert.Equal(4, structure.LatestProjectTasksCount);
				Assert.Equal(3, structure.UsersNotFinishedTasksCount);

				Assert.Equal(63, structure.UsersLongestDurationTask.Id);
				Assert.Equal(userId, structure.UsersLongestDurationTask.PerformerId);
			}
		}

		[Fact]
		public void GetUserProjectTasksInfo_WhenUserNotFound_ThenThrowNotFoundException()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();
				SeedDataForTask6(context);
				UsersQueryHandler usersQueryHandler = new UsersQueryHandler(context, mapper);

				int userId = int.MaxValue;
				Assert.Throws<NotFoundException>(() => usersQueryHandler.Handle(new GetUserProjectTasksInfoQuery { UserId = userId }));
			}
		}
		#endregion

		#region TASK 7
		// 7. �������� ���� ���������:
		// ������
		// ��������� ���� �������(�� ������)
		// ����������� ���� �������(�� �����)
		// �������� ������� ������������ � ������ �������, �� ��� ���� ������� >20 �������, ��� ������� ����� <3
		[Fact]
		public void GetProjectTasksTeamInfos_WhenThreeProjects_ThenCollectionOfProjectTaskTeamInfosOfThree()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();
				SeedDataForTask7(context);
				ProjectsQueryHandler projectsQueryHandler = new ProjectsQueryHandler(context, mapper);

				var structureCollection = projectsQueryHandler.Handle(new GetProjectTasksTeamInfosQuery());

				Assert.Collection(structureCollection,
					structure =>
					{
						Assert.Equal(71, structure.Project.Id);

						Assert.Equal(72, structure.LongestDescriptionTask.Id);
						Assert.Equal(72, structure.ShortestNameTask.Id);

						Assert.Equal(1, structure.TeamMembersCount);
					},
					structure =>
					{
						Assert.Equal(72, structure.Project.Id);

						Assert.Equal(74, structure.LongestDescriptionTask.Id);
						Assert.Equal(74, structure.ShortestNameTask.Id);

						Assert.Equal(1, structure.TeamMembersCount);
					},
					structure =>
					{
						Assert.Equal(73, structure.Project.Id);

						Assert.Null(structure.LongestDescriptionTask);
						Assert.Null(structure.ShortestNameTask);

						Assert.Equal(1, structure.TeamMembersCount);
					}
				);
			}
		}
		#endregion

		#region USER'S NOT FINISHED TASKS
		[Fact]
		public void GetNotFinishedUserTasks_WhenUserHasThreeTasksAndTwoOfThemNotFinished_ThenCollectionOfTwoTasks()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();
				SeedDataForNotFinishedUserTasks(context);

				var tasksQueryHandler = new TasksQueryHandler(context, mapper);

				int userId = 81;
				var notFinishedTasks = tasksQueryHandler.Handle(new GetNotFinishedUserTasksQuery { PerformerId = userId });

				Assert.Collection(notFinishedTasks,
					task =>
					{
						Assert.InRange(task.Id, 81, 82);
						Assert.Equal(userId, task.PerformerId);
						Assert.NotEqual(TaskState.Finished, task.State);
					},
					task =>
					{
						Assert.InRange(task.Id, 81, 82);
						Assert.Equal(userId, task.PerformerId);
						Assert.NotEqual(TaskState.Finished, task.State);
					}
				);
			}
		}

		[Fact]
		public void GetNotFinishedUserTasks_WhenUserDoesNotExist_ThenNotFoundException()
		{
			using (var context = new FakeCompanyDbContext(ContextOptions))
			{
				context.Database.EnsureDeleted();
				context.Database.EnsureCreated();

				var tasksQueryHandler = new TasksQueryHandler(context, mapper);

				int userId = int.MaxValue;

				Assert.Throws<NotFoundException>(() => tasksQueryHandler.Handle(new GetNotFinishedUserTasksQuery { PerformerId = userId }));
			}
		}
		#endregion
	}
}
